<?php

/**
 * This file saves the uploaded file from the user to the specified location or throws an exception in case of an  error.
 *
 * @package    BlueAcorn
 * @version    1.0.0
 * @author     Blue Acorn, Inc. <code@blueacorn.com>
 * @copyright  Copyright © 2014 Blue Acorn, Inc.
 */

class BlueAcorn_ImageOptions_Model_Resource_Eav_Attribute extends Mage_Catalog_Model_Resource_Eav_Attribute
{
    protected function _afterSave()
    {
        $data = $this->getData();

            if ($data)
            {
                if ( ($data['frontend_input'] == 'select') && ($data['enable_image_options'] == 1 ) && isset($data['option']))
                {
                    $validExtensionNames = array('jpg', 'jpeg', 'gif', 'png');

                    $optionsDelete=$data['option']['delete'];
                    $optionsValue=$data['option']['value'];

                    $path = Mage::helper('blueacorn_imageoptions')->getFlagAttributeLocation(); // upload path
                    if(!is_dir($path)) mkdir($path, 0777, true);


                    foreach ($optionsDelete as $optionId => $isDeleted)
                    {
                        $newFilename = 'option_'.$optionId.'.png';
                        if ( ($isDeleted == 1) && (file_exists($path.$newFilename)) ) unlink($path.$newFilename);
                    }

                    $takenOptionLabels = array();
                    foreach($optionsValue as $optionElement => $label)
                    {
                        $takenOptionLabels[] = $optionsValue[$optionElement][0];
                    }

                    foreach ($_FILES as $inputFieldName => $value)
                    {
                        if(count(array_keys($takenOptionLabels, $optionsValue[$inputFieldName][0])) > 1)
                        {
                            Mage::throwException('All file names must be unique, please try again.');
                        }

                        if($_FILES[$inputFieldName]['error'] == 0)
                        {
                            if ( (isset($_FILES[$inputFieldName]['name'])) && (file_exists($_FILES[$inputFieldName]['tmp_name'])) && ($optionsDelete[$inputFieldName] != 1) )
                            {
                                $uploader = new Varien_File_Uploader($inputFieldName);
                                $uploader->setAllowedExtensions($validExtensionNames);
                                $uploader->setAllowRenameFiles(false);
                                $uploader->setFilesDispersion(false);

                                if (!is_numeric($inputFieldName))
                                {
                                    if (is_array($optionsValue[$inputFieldName]))
                                        $optionId = $this->getSource()->getOptionId($optionsValue[$inputFieldName][0]);
                                    else
                                        $optionId = $this->getSource()->getOptionId($optionsValue[$inputFieldName]);
                                }
                                else $optionId=$inputFieldName;

                                $newFilename = 'option_'.$optionId.'.png';


                                $fileName = $_FILES[$inputFieldName]['name'];
                                $ext = pathinfo($fileName, PATHINFO_EXTENSION);
                                if( !in_array($ext, $validExtensionNames) )
                                {
                                    Mage::throwException('This file does not seem to be an image with a valid file extension please make sure it is one of these file types .jpg/.jpeg/.gif/.png and try again.');
                                }
                                else
                                {
                                    $image = imagecreatefromstring(file_get_contents($_FILES[$inputFieldName]['tmp_name']));
                                    imagealphablending($image, true);
                                    imagesavealpha($image, true);

                                    if (!imagepng($image, $path.$newFilename))
                                    {
                                        Mage::throwException('Invalid image file. Please make sure you select a valid image file for upload.');
                                    }
                                }
                            }
                        }
                        elseif($_FILES[$inputFieldName]['error'] == 4)
                        {
                            //There is two reasons for this, no image was uploaded because:
                            //  1: the user didnt want to upload an image for a new option.
                            //  2: a new image wasnt uploaded to an option that already exists.
                            //Either way, I dont have any reason to prevent this action so this is just a placeholder for now.
                            //todo: Custom code could be placed here to prevent the user from electing to not add an image to an option.
                        }
                        else
                        {
                            Mage::throwException('There was an issue saving your image. Make sure the size is less than 2mb. and that it is one of these file types .jpg/.jpeg/.gif/.png .');
                        }

                    }
                }
            }
        return parent::_afterSave();
    }
}