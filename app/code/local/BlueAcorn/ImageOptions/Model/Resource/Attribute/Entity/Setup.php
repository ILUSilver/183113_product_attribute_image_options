<?php

/**
 * This setup adds enable_image_options attribute/value to the array of values to save.
 *
 * @package    BlueAcorn
 * @version    1.0.0
 * @author     Blue Acorn, Inc. <code@blueacorn.com>
 * @copyright  Copyright © 2014 Blue Acorn, Inc.
 */

class BlueAcorn_ImageOptions_Model_Resource_Attribute_Entity_Setup extends Mage_Catalog_Model_Resource_Setup
{
    protected function _prepareValues($attr)
    {
        $data = parent::_prepareValues($attr);
        $data = array_merge($data, array(
            'enable_image_options'       => $this->_getValue($attr, 'enable_image_options', 0)
        ));
        return $data;
    }
}