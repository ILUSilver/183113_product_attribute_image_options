<?php

/**
 * This observer adds the image options field to the bottom of the attribute edit grid,
 *      allowing the user to enable/disable the use of flags for each attribute option.
 *
 * @package    BlueAcorn
 * @version    1.0.0
 * @author     Blue Acorn, Inc. <code@blueacorn.com>
 * @copyright  Copyright © 2014 Blue Acorn, Inc.
 */

class BlueAcorn_ImageOptions_Model_Observer
{
    /**
     * Event observed = adminhtml_catalog_product_attribute_edit_prepare_form
     * 
     */
    public function addImageOptionsField(Varien_Event_Observer $observer)
    {
        $form = $observer->getForm();

        /* @var $fieldset Varien_Data_Form_Element_Fieldset */
        $fieldset = $form->getElement('front_fieldset');
        $fieldset->addField('enable_image_options', 'select', array(
            'name' => 'enable_image_options',
            'label' => Mage::helper('catalog')->__('Enable Image Options'),
            'title' => Mage::helper('catalog')->__('Enable Image Options'),
            'note'  => Mage::helper('catalog')->__('You MUST save your attribute for this to take effect'),
            'values' => array(
                array('value' => '0', 'label' => Mage::helper('catalog')->__('No')),
                array('value' => '1', 'label' => Mage::helper('catalog')->__('Yes'))
            ),
        ));


        $layout = Mage::getSingleton('core/layout');
        $dependenciesBlock=$layout->createBlock('adminhtml/widget_form_element_dependence')
                ->addFieldMap("enable_image_options", 'enable_image_options')
                ->addFieldMap("frontend_input", 'frontend_input_type')
                ->addFieldDependence('enable_image_options', 'frontend_input_type', 'select');
        $layout->getBlock('content')->append($dependenciesBlock);
    }

}
