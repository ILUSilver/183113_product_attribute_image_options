<?php

/**
 * This helper file can grab the pre-defined image extension as well as look for and return the
 *      html to show an attribute option's flag.
 *
 * @package    BlueAcorn
 * @version    1.0.0
 * @author     Blue Acorn, Inc. <code@blueacorn.com>
 * @copyright  Copyright © 2014 Blue Acorn, Inc.
 */

class BlueAcorn_ImageOptions_Helper_Data extends Mage_Core_Helper_Abstract
{

    const FLAG_ATTRIBUTE_IMAGE_EXT = '.png';

    public function getFlagAttributeLocation($url = false){
        $location = ($url) ?  Mage::getBaseurl('media') : Mage::getBaseDir('media') . DS;
        return $location . 'blueacorn_imageoptions' . DS;
    }


    protected function getFlagAttributeHtml($optionId = false, $optionLabel = false)
    {
        $html = '';
        $flagImageDirectory = $this->getFlagAttributeLocation() .'option_';
        if($optionId && $optionLabel && file_exists($flagImageDirectory . $optionId . self::FLAG_ATTRIBUTE_IMAGE_EXT))
        {
            $html = '<div class="flag-attribute ' . $optionLabel . '">';
            $html .= '<img src="' . $this->getFlagAttributeLocation(true) .'option_' . $optionId . self::FLAG_ATTRIBUTE_IMAGE_EXT . '">';
            $html .= '</div>';
        }

        return $html;
    }

    public function showSpecificAttributeFlagForProduct($_product = null, $flagCode = null)
    {
        $html = '';

        if($_product && $_product->getId() && $flagCode && $_product->getData($flagCode))
        {
                $optionLabel = $_product->getAttributeText($flagCode);
                $optionId = $_product->getData($flagCode);

                $html = $this->getFlagAttributeHtml($optionId, $optionLabel);
        }
        return $html;
    }
}

