<?php

/**
 * This block creates the form that will be used when displaying the admin. .phtml.
 *
 * @package    BlueAcorn
 * @version    1.0.0
 * @author     Blue Acorn, Inc. <code@blueacorn.com>
 * @copyright  Copyright © 2014 Blue Acorn, Inc.
 */

class BlueAcorn_ImageOptions_Block_Adminhtml_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array('id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post', 'enctype' => 'multipart/form-data'));
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }

}
