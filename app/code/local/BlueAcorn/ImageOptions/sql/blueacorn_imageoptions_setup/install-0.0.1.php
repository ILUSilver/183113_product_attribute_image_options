<?php
/**
 * An install script to add a column to the catalog/eav_attribute table, called 'enable_image_options'.
 *
 * @package    BlueAcorn
 * @version    1.0.0
 * @author     Blue Acorn, Inc. <code@blueacorn.com>
 * @copyright  Copyright © 2014 Blue Acorn, Inc.
 */


$installer=$this;

$installer->startSetup();


$installer->getConnection()->addColumn($installer->getTable('catalog/eav_attribute'), 'enable_image_options', Varien_Db_Ddl_Table::TYPE_TINYINT, null,
                                       array('comment' => 'Enable image options attributes for dropdown type fields'));


$installer->endSetup();
