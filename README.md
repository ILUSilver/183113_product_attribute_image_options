Blue Acorn:
===========
*Product Attribute Options Images - Flags*
----------------------------------------
---

[User Guide Link](https://docs.google.com/a/blueacorn.com/document/d/1-EVYyloUnLFqa5C-_9RSTeXwi-8_2G87NI8bXA0unB4/edit#heading=h.qtaqcnep6g5y)

Version:
--------
1.0.0

Summary:
--------
> This module allows the client to, after defining a custom attribute for a product, assign an image to an option of that custom attribute. This then allows the client to show an image for a product on both the product view and product list pages (or wherever defined in the .phtml).

Requirements:
-------------
 - Magento Enterprise Edition (EE) Version 1.14.0 + 
 - A Product has been created.
 - An attribute has been created.
 - The above attribute has been assigned to an attribute set.
 - The product above has been assigned to the afore mentioned attribute set.
 - There are no dependencies to note.

Installation Instructions:
--------------------------
> __Install Magento/Prepare Site Instance__:
 - If needed, install a new instance of Magento EE v.1.1.4.0 +
 - In this README.md's directory, copy and merge the App and Media folder into your             instance's root directory.
 - You may need to clear the cache and reindex your instance (Admin. Panel).
 

Configuration Instructions:
---------------------------
> __3 Parts__:
 - Set-up Product and Attribute/set
 - Update .phtml Page.
 - View Frontend Page


> __Set-up Product and Attribute/set__:
 - Navigate to the Administration Panel for your instance.
 - If desired, create a new attribute.
    - Make sure that the created attribute has 'Dropdown' selected for 'catalog         input type for store owner'.
 - Select the desired attribtute you would like to add image flags too.
 - Look under the frontend properties section for the "Enable Image Options"         option and set it to 'yes'(true).
 - Save and then select the same attribute again.
 - Navigate to the 'Manage Label/Options tab along the left side of the screen.
 - There should now be a section on the page called 'Manage Image Options'.
 - Select the 'add image option' button.
 - Add the code(name) of the option you want to add and the current value
   (flag/image) you want to represent the option.
 - Repeat the above for as many options as you would like.
 - When finished, Make sure to save the attribute once more.
 - Next, if the attribute you added options too is not assigned to an attribute      set, assign the attribute to a desired attribute set.
 - Assign the attribute set to a new product if needed.
 - Navigate the product desired, and select an option for the attribute that you      want the product to have. 
 - Once finished, save the product.
 
> __Update .phtml Page__:
 - Figure out the .phtml file name of the page you would like to modify.
 - Add the appropriate code to the .phtml file.
    - (ex. echo Mage::helper('blueacorn_imageoptions')
        ->showSpecificAttributeFlagForProduct(Mage::getModel('catalog/product')
        ->load(3),'color');)
 - If desired, add/modify the appropriate .css file with any visual changes you      would like to make to the project, as this project is only the barebones          meaning that NO STYLING is done at all.
 
> __View Frontend Page__:
 - Navigate to the page that you modified earlier.
 - Modify the aforementioned .phtml page to the clients needs if necessary.
 - *Remember to clear cache.



Technical Description:
----------------------
> __This module__:
   - Uses the php gd library.
   - Add's an attribute to the EAV table.
   - Images that are saved for an option are saved into the media/blueacorn_imageoptions folder.
   - The only validation (besides required entry/is empty) that has been added to the admin. panel when naming the option codes is to test for uniqueness.








Possible Issues:
----------------

It may be that when trying to add an image to an attribute option, a message appears that mention that a folder was unable to be made in the media folder. To remedy this, just change the permissions of that folder in your instances root directory(possibly needs to be 777).



Contact Information:
--------------------
If any bugs/comments/complaints, please contact either of us with the appropriate subject line at : 
 - ___jav.ivanov@blueacorn.com___  
 - ___gabriell.juliana@blueacorn.com___ 




